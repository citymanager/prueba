<?php

namespace Tests\Feature;

use App\Models\Client;
use Tests\TestCase;

class ClientTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_all_clients()
    {
        $response = $this->json('GET', '/api/v1/clients');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function it_can_create_an_client()
    {
        $data = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'document' => $this->faker->ean8,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->ean13
        ];

        $response = $this->json('POST', '/api/v1/clients', $data);
        $response
            ->assertStatus(201)
            ->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_requires_all_attributes_register_client()
    {
        $response = $this->json('POST', '/api/v1/clients');
        $response->assertStatus(422)
            ->assertJson([
                "error" => [
                    'first_name' => ['El nombre es requerido'],
                    'last_name' => ['El apellido es requerido'],
                    'document' => ['El número de documento es requerido'],
                    'email' => ['El email es requerido'],
                    'phone' => ['El teléfono es requerido']
                ],
                "code" => 422
            ]);
    }

    /**
     * @test
     */
    public function it_can_detail_of_the_client()
    {
        $client = factory(Client::class)->create();

        $response = $this->json('GET', '/api/v1/clients/' . $client->id);
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'first_name' => $client->first_name
            ]);

    }

    /**
     * @test
     */
    public function it_can_updated_of_the_client()
    {
        $updated = factory('App\Models\Client')->create();

        $updated->first_name = 'Simon';

        $response = $this->json('PUT', '/api/v1/clients/' . $updated->id, $updated->toArray());
        $response->assertStatus(200);

        $this->assertDatabaseHas('clients', ['id' => $updated->id, 'first_name' => 'Simon']);
    }

    /**
     * @test
     */
    public function it_can_deleted_an_client()
    {
        $data = factory(Client::class)->create();

        $response = $this->json('DELETE', '/api/v1/clients/' . $data->id);
        $response->assertStatus(200);
    }
}
