<?php

namespace Tests\Unit;

use App\Models\Client;
use App\Repositories\ClientRepository;
use PHPUnit\Util\ErrorHandler;
use Tests\TestCase;


class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function it_can_create_the_client()
    {
        $data = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'document' => $this->faker->ean8,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->ean13
        ];

        $clientRepo = new ClientRepository(new Client);
        $client = $clientRepo->store($data);

        $this->assertInstanceOf(Client::class, $client);
        $this->assertEquals($data['first_name'], $client->first_name);
        $this->assertEquals($data['last_name'], $client->last_name);
        $this->assertEquals($data['document'], $client->document);
        $this->assertEquals($data['email'], $client->email);
        $this->assertEquals($data['phone'], $client->phone);
    }

    /**
     * @test
     */
    public function it_can_detail_the_client()
    {
        $client = factory(Client::class)->create();
        $clientRepo = new ClientRepository(new Client);
        $data = $clientRepo->show($client->id);

        $this->assertInstanceOf(Client::class, $data);
        $this->assertEquals($data->first_name, $client->first_name);
        $this->assertEquals($data->last_name, $client->last_name);
        $this->assertEquals($data->document, $client->document);
        $this->assertEquals($data->email, $client->email);
        $this->assertEquals($data->phone, $client->phone);
    }

    /**
     * @test
     */
    /*public function it_can_update_the_client()
    {
        $client = factory(Client::class)->create();

        $id = $client->id;

        $data = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'document' => $this->faker->ean8,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->ean13
        ];

        $clientRepo = new ClientRepository($client);
        $update = $clientRepo->update($data, $id);

        $this->assertTrue((boolean)$update);
        $this->assertEquals($data['first_name'], $client->first_name);
        $this->assertEquals($data['last_name'], $client->last_name);
        $this->assertEquals($data['document'], $client->document);
        $this->assertEquals($data['email'], $client->email);
        $this->assertEquals($data['phone'], $client->phone);
    }*/
}
