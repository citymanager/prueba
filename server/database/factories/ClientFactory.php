<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(\App\Models\Client::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'document' => $faker->unique()->ean8,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->ean13
    ];
});
