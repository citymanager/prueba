<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'document' => 'required|unique:clients,document,' . $this->client,
            'email' => 'required|email|unique:clients,email,' . $this->client,
            'phone' => 'required'
        ];

    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'El :attribute es requerido',
            'last_name.required' => 'El :attribute es requerido',
            'document.required' => 'El :attribute es requerido',
            'document.unique' => 'El :attribute ya existe en el sistema',
            'email.required' => 'El :attribute es requerido',
            'email.unique' => 'El :attribute ya existe en el sistema',
            'email.email' => 'El :attribute tiene un formato no valido',
            'phone.required' => 'El :attribute es requerido'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'document' => 'número de documento',
            'email' => 'email',
            'phone' => 'teléfono'
        ];
    }
}
