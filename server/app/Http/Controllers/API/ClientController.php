<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Interfaces\ClientInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientFormRequest;
use App\Http\Resources\Client as Resource;
use App\Http\Requests\ClientUpdateFormRequest;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    private $repository;

    /**
     * ClientController constructor.
     * @param ClientInterface $repository
     */
    public function __construct(ClientInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        return $this->repository->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientFormRequest $request
     * @return Resource
     */
    public function store(ClientFormRequest $request)
    {
        $client = $this->repository->store($request->validated());

        return new Resource($client);
    }

    /**
     * Display the specified resource.
     *
     * @param $client
     * @return mixed
     */
    public function show($client)
    {
        return new Resource($this->repository->show($client));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientUpdateFormRequest $request
     * @param $id
     * @return Resource
     */
    public function update(ClientUpdateFormRequest $request, $id)
    {
        $this->repository->update($request->all(), $id);

        return $this->repository->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->repository->delete($id);
    }
}
