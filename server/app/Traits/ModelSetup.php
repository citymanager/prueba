<?php


namespace App\Traits;


trait ModelSetup
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected function initAttributes()
    {
        if(property_exists($this, 'guarded')) $this->guarded = [];
    }
}
