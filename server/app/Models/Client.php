<?php

namespace App\Models;

use App\Traits\ModelSetup;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use ModelSetup;

    /**
     * Client constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->initAttributes();
        parent::__construct($attributes);
    }
}
