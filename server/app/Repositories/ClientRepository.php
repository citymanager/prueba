<?php /** @noinspection ALL */


namespace App\Repositories;

use App\Interfaces\ClientInterface;
use App\Models\Client as Model;

class ClientRepository implements ClientInterface
{

    protected $model;

    /**
     * ClientRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed|void
     */
    public function index()
    {
        return $this->model->all();
    }

    /**
     * @param $clientData
     * @return Client
     */
    public function store(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        return $this->model->where('id', $id)->update($data);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
}
