import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Client } from "../interfaces/client";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ClientService {
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http
      .get(`${environment.url}/clients`, { headers: this.headers })
      .pipe(map(clients => clients));
  }

  create(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.url}/clients`, client, {
      headers: this.headers
    });
  }

  getById(id: number) {
    return this.http.get<Client>(`${environment.url}/clients/${id}`, {
      headers: this.headers
    });
  }

  update(client) {
    return this.http.put<Client>(
      `${environment.url}/clients/${client.id}`,
      client,
      { headers: this.headers }
    );
  }

  delete(id: number) {
    return this.http.delete<Client>(`${environment.url}/clients/${id}`, {
      headers: this.headers
    });
  }
}
