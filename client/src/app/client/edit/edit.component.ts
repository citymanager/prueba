import { Component, OnInit } from "@angular/core";
import { ClientService } from "../../providers/client.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Client } from "../../interfaces/client";
import { ToastrManager } from "ng6-toastr-notifications";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class EditComponent implements OnInit {
  public error: any = [];
  registerForm: FormGroup;

  client: Client = {
    id: null,
    first_name: "",
    last_name: "",
    document: null,
    email: "",
    phone: null
  };

  id: any = "";

  constructor(
    private service: ClientService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrManager
  ) {
    const id: number = activatedRoute.snapshot.params.id;
    this.getClient(id);
  }

  ngOnInit() {}

  getClient(id) {
    return this.service
      .getById(id)
      .subscribe(client => (this.client = client["data"]));
  }

  onUpdate(form: NgForm) {
    this.service
      .update(form.value)
      .subscribe(
        data => this.handleResponse(),
        error => this.handleError(error)
      );
  }

  handleResponse() {
    this.toastr.successToastr(
      "El usuario se actualizó correctamente",
      "Felicitaciones"
    );
    this.router.navigate(["/clients"]);
  }

  handleError(error) {
    const resultError = error.error.error;
    const transform = Object["values"](resultError);
    let object = {};
    const array = [];
    transform.map(item => {
      object = item;
      array.push(object);
    });

    this.error = array;
  }
}
