import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ClientService } from "../../providers/client.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { ToastrManager } from "ng6-toastr-notifications";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"]
})
export class CreateComponent implements OnInit {
  public error: any[];
  client: any;

  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  numberPattern: any = /^[0-9]+$/;
  createFormGroup() {
    return new FormGroup({
      first_name: new FormControl("", [Validators.required]),
      last_name: new FormControl("", [Validators.required]),
      document: new FormControl("", [
        Validators.required,
        Validators.pattern(this.numberPattern)
      ]),
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(this.emailPattern)
      ]),
      phone: new FormControl("", [
        Validators.required,
        Validators.pattern(this.numberPattern)
      ])
    });
  }

  clientForm: FormGroup;

  constructor(
    private router: Router,
    private service: ClientService,
    private toastr: ToastrManager
  ) {
    this.clientForm = this.createFormGroup();
  }

  ngOnInit() {}

  onSubmit() {
    this.service
      .create(this.clientForm.value)
      .subscribe(
        data => this.handleResponse(),
        error => this.handleError(error)
      );
  }

  get first_name() {
    return this.clientForm.get("first_name");
  }
  get last_name() {
    return this.clientForm.get("last_name");
  }
  get document() {
    return this.clientForm.get("document");
  }
  get email() {
    return this.clientForm.get("email");
  }
  get phone() {
    return this.clientForm.get("phone");
  }

  handleResponse() {
    this.toastr.successToastr(
      "El usuario se creó correctamente",
      "Felicitaciones"
    );
    this.router.navigate(["/clients"]);
  }

  handleError(error) {
    const resultError = error.error.error;
    const transform = Object["values"](resultError);
    let object = {};
    const array = [];
    transform.map(item => {
      object = item;
      array.push(object);
    });

    this.error = array;
  }
}
