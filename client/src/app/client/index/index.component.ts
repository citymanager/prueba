import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ClientService } from "../../providers/client.service";
import { ToastrManager } from "ng6-toastr-notifications";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.scss"]
})
export class IndexComponent implements OnInit {
  page: number = 1;
  clients: any;
  filter = "";

  constructor(
    private service: ClientService,
    private router: Router,
    private toastr: ToastrManager,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.getClients();
  }

  getClients() {
    /** spinner starts on init */
    this.spinner.show();
    this.service.getAll().subscribe(clients => {
      this.clients = clients;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 2000);
    });
  }

  create() {
    /** spinner starts on init */
    this.spinner.show();
    this.service.getAll().subscribe(clients => {
      this.clients = clients;
      setTimeout(() => {
        this.router.navigate(["/clients/create"]);
        this.spinner.hide();
      }, 2000);
    });
  }

  update(id) {
    /** spinner starts on init */
    this.spinner.show();
    setTimeout(() => {
      this.router.navigate([`/clients/edit/${id}`]);
      this.spinner.hide();
    }, 2000);
  }

  removeClient(id: number) {
    this.service.delete(id).subscribe(data => this.handleResponse());
  }

  handleResponse() {
    this.toastr.successToastr(
      "El cliente se eliminó correctamente",
      "Felicitaciones"
    );
    this.getClients();
  }
}
