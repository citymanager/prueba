export interface Client {
  id: number;
  first_name: string;
  last_name: string;
  document: number;
  email: string;
  phone: number;
}
