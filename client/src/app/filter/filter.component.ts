import { Component, OnInit } from "@angular/core";
import { ClientService } from "../providers/client.service";

@Component({
  selector: "app-filter",
  templateUrl: "./filter.component.html",
  styleUrls: ["./filter.component.scss"]
})
export class FilterComponent implements OnInit {
  constructor(private service: ClientService) {}
  clients: any;
  ngOnInit() {
    this.getClients();
  }

  getClients() {
    this.service
      .getAll()
      .subscribe(clients => (this.clients = clients["clients"]));
  }
}
