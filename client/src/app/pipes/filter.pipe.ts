import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "filter"
})
export class FilterPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        if (args === "" || args.length < 2) {
            return value;
        }

        const result = [];
        for (const client of value) {
            if (client.first_name.toLowerCase().indexOf(args.toLowerCase()) > -1) {
                result.push(client);
            }
        }
        return result;
    }
}